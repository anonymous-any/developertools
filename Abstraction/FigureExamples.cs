﻿namespace Abstraction
{
    using System;
    using log4net;
    using log4net.Appender;
    using log4net.Config;

    /// <summary>
    /// Figure examples.
    /// </summary>
    public static class FigureExamples
    {
        /// <summary>
        /// Define a static logger variable so that it references the Logger instance named
        /// "FigureExamples".
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(FigureExamples));

        /// <summary>
        /// Main entry-point for this application.
        /// </summary>
        public static void Main()
        {
            var consoleLayout = new log4net.Layout.PatternLayout("%date: [%thread] %-5level %logger -- %message%newline");
            var consoleAppender = new ConsoleAppender()
            {
                Layout = consoleLayout
            };
            BasicConfigurator.Configure(consoleAppender);

            Log.Warn("Program starts to execute code.");
            Log.Debug("Line 24 executed.");

            Circle circle = new Circle(5);
            Log.Info(string.Format(
                "I am a circle. My perimeter is {0:f2}. My surface is {1:f2}.",
                circle.CalcPerimeter(),
                circle.CalcSurface()));

            Rectangle rect = new Rectangle(2, 3);
            Log.Info(string.Format(
                "I am a rectangle. My perimeter is {0:f2}. My surface is {1:f2}.",
                rect.CalcPerimeter(),
                rect.CalcSurface()));

            TryCatchAction(() =>
            {
                Log.Info("Try to create Circle with radius = 0");
                new Circle(0);
            });

            TryCatchAction(() =>
            {
                Log.Info("Try to create Rectangle with width = 0");
                new Rectangle(0, 3);
            });
            TryCatchAction(() =>
            {
                Log.Info("Try to create Rectangle with height = 0");
                new Rectangle(2, 0);
            });
        }

        /// <summary>
        /// Try catch action.
        /// </summary>
        /// <param name="action">The action.</param>
        private static void TryCatchAction(Action action)
        {
            try
            {
                action();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}