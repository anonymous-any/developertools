﻿namespace Abstraction
{
    /// <summary>
    /// Abstract class Figure.
    /// </summary>
    public abstract class Figure
    {
        /// <summary>
        /// Calculates the perimeter.
        /// </summary>
        /// <returns>
        /// The calculated perimeter.
        /// </returns>
        public abstract double CalcPerimeter();

        /// <summary>
        /// Calculates the surface.
        /// </summary>
        /// <returns>
        /// The calculated surface.
        /// </returns>
        public abstract double CalcSurface();
    }
}