﻿namespace Abstraction
{
    using System;

    /// <summary>
    /// Representing a Circle figure.
    /// </summary>
    /// <seealso cref="T:Abstraction.Figure"/>
    public class Circle : Figure
    {
        /// <summary>
        /// The radius.
        /// </summary>
        private double radius;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Abstraction.Circle"/> class.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown when one or more arguments are outside the required range.
        /// </exception>
        /// <param name="radius">The radius.</param>
        public Circle(double radius)
        {
            if (radius > 0)
            {
                this.Radius = radius;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Invalid input data. Radius must be positive!");
            }
        }

        /// <summary>
        /// Gets or sets the radius.
        /// </summary>
        /// <value>
        /// The radius.
        /// </value>
        public double Radius
        {
            get
            {
                return this.radius;
            }

            set
            {
                if (value > 0)
                {
                    this.radius = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid input data. Radius must be positive!");
                }
            }
        }

        /// <summary>
        /// Calculates the perimeter.
        /// </summary>
        /// <returns>
        /// The calculated perimeter.
        /// </returns>
        public override double CalcPerimeter()
        {
            double perimeter = 2 * Math.PI * this.Radius;

            return perimeter;
        }

        /// <summary>
        /// Calculates the surface.
        /// </summary>
        /// <returns>
        /// The calculated surface.
        /// </returns>
        public override double CalcSurface()
        {
            double surface = Math.PI * this.Radius * this.Radius;

            return surface;
        }
    }
}