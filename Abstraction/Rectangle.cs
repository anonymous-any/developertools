﻿namespace Abstraction
{
    using System;

    /// <summary>
    /// Representing a Rectangle figure.
    /// </summary>
    /// <seealso cref="T:Abstraction.Figure"/>
    public class Rectangle : Figure
    {
        /// <summary>
        /// The width.
        /// </summary>
        private double width;

        /// <summary>
        /// The heigth.
        /// </summary>
        private double heigth;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Abstraction.Rectangle"/> class.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown when one or more arguments are outside the required range.
        /// </exception>
        /// <param name="width"> The width.</param>
        /// <param name="height">The height.</param>
        public Rectangle(double width, double height)
        {
            if (width > 0)
            {
                this.Width = width;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Invalid input data. Width must be positive!");
            }

            if (height > 0)
            {
                this.Height = height;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Invalid input data. Heigth must be positive!");
            }
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public double Width
        {
            get
            {
                return this.width;
            }

            set
            {
                if (value > 0)
                {
                    this.width = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid input data. Width must be positive!");
                }
            }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public double Height
        {
            get
            {
                return this.heigth;
            }

            set
            {
                if (value > 0)
                {
                    this.heigth = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid input data. Height must be positive!");
                }
            }
        }

        /// <summary>
        /// Calculates the perimeter.
        /// </summary>
        /// <returns>
        /// The calculated perimeter.
        /// </returns>
        public override double CalcPerimeter()
        {
            double perimeter = 2 * (this.Width + this.Height);
            return perimeter;
        }

        /// <summary>
        /// Calculates the surface.
        /// </summary>
        /// <returns>
        /// The calculated surface.
        /// </returns>
        public override double CalcSurface()
        {
            double surface = this.Width * this.Height;
            return surface;
        }
    }
}